from influxdb import InfluxDBClient
from kafka import KafkaConsumer
import json
import sys


client = InfluxDBClient('afa86e20f472311eabea406ab80264e3-1036800842.ap-south-1.elb.amazonaws.com', '8086', database='first')
cons = KafkaConsumer('new23', bootstrap_servers="a96005074419711eabea406ab80264e3-950000899.ap-south-1.elb.amazonaws.com:9092")
for msg in cons:
    entry = msg.value
    json_body = [{
        "measurement" : "testing",
        "tags":{
            "user": entry
        },
        "time": "2020-02-02T23:00:00Z",
        "fields": {
            'entry': entry
        }
    }
    ]
    client.write_points(json_body)
    result = client.query("SELECT entry from testing;")
    print(result)